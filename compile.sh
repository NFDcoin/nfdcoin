#!/bin/sh
CP=conf/:classes/:lib/*
SP=src/java/

/bin/rm -f nfd.jar
/bin/rm -rf classes
/bin/mkdir -p classes/

javac -sourcepath $SP -classpath $CP -d classes/ src/java/nxt/*.java src/java/nxt/*/*.java || exit 1

jar cfm nfd.jar MANIFEST.MF -C classes . || exit 1

echo "nfd.jar and classes generated successfully"
