/**
 * @depends {nrs.js}
 */
var NRS = (function (NRS, $, undefined) {
    NRS.lastTransactions = "";

    NRS.unconfirmedTransactions = [];
    NRS.unconfirmedTransactionIds = "";
    NRS.unconfirmedTransactionsChange = true;

    NRS.transactionsPageType = null;

    NRS.getInitialTransactions = function () {
        NRS.sendRequest("getAccountTransactions", {
            "account": NRS.account,
            "firstIndex": 0,
            "lastIndex": 9
        }, function (response) {
            if (response.transactions && response.transactions.length) {
                var transactions = [];
                var transactionIds = [];

                for (var i = 0; i < response.transactions.length; i++) {
                    var transaction = response.transactions[i];

                    transaction.confirmed = true;
                    transactions.push(transaction);

                    transactionIds.push(transaction.transaction);
                }

                NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                    NRS.handleInitialTransactions(transactions.concat(unconfirmedTransactions), transactionIds);
                });
            }

            NRS.handleInitialTransactions = function (transactions, transactionIds) {
                if (transactions.length) {
                    var rows = "";

                    transactions.sort(NRS.sortArray);

                    if (transactionIds.length) {
                        NRS.lastTransactions = transactionIds.toString();
                    }

                    for (var i = 0; i < transactions.length; i++) {
                        var transaction = transactions[i];

                        var receiving = transaction.recipient == NRS.account;

                        var account = (receiving ? "sender" : "recipient");

                        if (transaction.amountNQT) {
                            transaction.amount = new BigInteger(transaction.amountNQT);
                            transaction.fee = new BigInteger(transaction.feeNQT);
                        }

                        rows += "<tr class='" + (!transaction.confirmed ? "tentative" : "confirmed") + "'><td><a href='#' data-transaction='" + String(transaction.transaction).escapeHTML() + "' data-timestamp='" + String(transaction.timestamp).escapeHTML() + "'>" + NRS.formatTimestamp(transaction.timestamp) + "</a></td><td style='width:5px;padding-right:0;'>" + (transaction.type == 0 ? (receiving ? "<i class='fa fa-plus-circle' style='color:#65C62E'></i>" : "<i class='fa fa-minus-circle' style='color:#E04434'></i>") : "") + "</td><td><span" + (transaction.type == 0 && receiving ? " style='color:#006400'" : (!receiving && transaction.amount > 0 ? " style='color:red'" : "")) + ">" + NRS.formatAmount(transaction.amount) + "</span> <span" + ((!receiving && transaction.type == 0) ? " style='color:red'" : "") + ">+</span> <span" + (!receiving ? " style='color:red'" : "") + ">" + NRS.formatAmount(transaction.fee) + "</span></td><td>" + NRS.getAccountLink(transaction, account) + "</td><td class='confirmations' data-confirmations='" + String(transaction.confirmations).escapeHTML() + "' data-content='" + NRS.formatAmount(transaction.confirmations) + " confirmations' data-container='body' data-initial='true'>" + (transaction.confirmations > 10 ? "10+" : String(transaction.confirmations).escapeHTML()) + "</td></tr>";
                    }

                    $("#dashboard_transactions_table tbody").empty().append(rows);
                }

                NRS.dataLoadFinished($("#dashboard_transactions_table"));
            }

            NRS.getNewTransactions = function () {
                //check if there is a new transaction..
                NRS.sendRequest("getAccountTransactionIds", {
                    "account": NRS.account,
                    "timestamp": NRS.blocks[0].timestamp + 1,
                    "firstIndex": 0,
                    "lastIndex": 0
                }, function (response) {
                    //if there is, get latest 10 transactions
                    if (response.transactionIds && response.transactionIds.length) {
                        NRS.sendRequest("getAccountTransactions", {
                            "account": NRS.account,
                            "firstIndex": 0,
                            "lastIndex": 9
                        }, function (response) {
                            if (response.transactions && response.transactions.length) {
                                var transactionIds = [];

                                $.each(response.transactions, function (key, transaction) {
                                    transactionIds.push(transaction.transaction);
                                    response.transactions[key].confirmed = true;
                                });

                                NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                                    NRS.handleIncomingTransactions(response.transactions.concat(unconfirmedTransactions), transactionIds);
                                });
                            } else {
                                NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                                    NRS.handleIncomingTransactions(unconfirmedTransactions);
                                });
                            }
                        });
                    } else {
                        NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                            NRS.handleIncomingTransactions(unconfirmedTransactions);
                        });
                    }
                });
            }
        });
    }

    NRS.handleInitialTransactions = function (transactions, transactionIds) {
        if (transactions.length) {
            var rows = "";

            transactions.sort(NRS.sortArray);

            if (transactions.length >= 1) {
                NRS.lastTransactions = transactionIds.toString();

                for (var i = transactions.length - 1; i >= 0; i--) {
                    if (transactions[i].confirmed) {
                        NRS.lastTransactionsTimestamp = transactions[i].timestamp;
                        break;
                    }
                }
            }

            for (var i = 0; i < transactions.length; i++) {
                var transaction = transactions[i];

                var receiving = transaction.recipient == NRS.account;

                var account = (receiving ? "sender" : "recipient");

                if (transaction.amountNQT) {
                    transaction.amount = new BigInteger(transaction.amountNQT);
                    transaction.fee = new BigInteger(transaction.feeNQT);
                }

                rows = "<tr class='" + (!transaction.confirmed ? "tentative" : "confirmed") + "'><td><a href='#' data-transaction='" + String(transaction.transaction).escapeHTML() + "' data-timestamp='" + String(transaction.timestamp).escapeHTML() + "'>" + NRS.formatTimestamp(transaction.timestamp) + "</a></td><td style='width:5px;padding-right:0;'>" + (transaction.type == 0 ? (receiving ? "<i class='fa fa-plus-circle' style='color:#65C62E'></i>" : "<i class='fa fa-minus-circle' style='color:#E04434'></i>") : "") + "</td><td><span" + (transaction.type == 0 && receiving ? " style='color:#006400'" : (!receiving && transaction.amount > 0 ? " style='color:red'" : "")) + ">" + NRS.formatAmount(transaction.amount) + "</span> <span" + ((!receiving && transaction.type == 0) ? " style='color:red'" : "") + ">+</span> <span" + (!receiving ? " style='color:red'" : "") + ">" + NRS.formatAmount(transaction.fee) + "</span></td><td>" + NRS.getAccountLink(transaction, account) + "</td><td class='confirmations' data-confirmations='" + String(transaction.confirmations).escapeHTML() + "' data-content='" + NRS.formatAmount(transaction.confirmations) + " confirmations' data-container='body' data-initial='true'>" + (transaction.confirmations > 10 ? "10+" : String(transaction.confirmations).escapeHTML()) + "</td></tr>" + rows;
            }

            $("#dashboard_transactions_table tbody").empty().append(rows);
        }

        NRS.dataLoadFinished($("#dashboard_transactions_table"));
    }

    NRS.getNewTransactions = function () {
        NRS.sendRequest("getAccountTransactionIds", {
            "account": NRS.account,
            "timestamp": NRS.lastTransactionsTimestamp
        }, function (response) {
            if (response.transactionIds && response.transactionIds.length) {
                var transactionIds = response.transactionIds.slice(0, 10);

                if (transactionIds.toString() == NRS.lastTransactions) {
                    NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                        NRS.handleIncomingTransactions(unconfirmedTransactions);
                    });
                    return;
                }

                NRS.transactionIds = transactionIds;

                var nrTransactions = 0;

                var newTransactions = [];

                //if we have a new transaction, we just get them all.. (10 max)
                for (var i = 0; i < transactionIds.length; i++) {
                    NRS.sendRequest("getTransaction", {
                        "transaction": transactionIds[i]
                    }, function (transaction, input) {
                        nrTransactions++;

                        transaction.transaction = input.transaction;
                        transaction.confirmed = true;
                        newTransactions.push(transaction);

                        if (nrTransactions == transactionIds.length) {
                            NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                                NRS.handleIncomingTransactions(newTransactions.concat(unconfirmedTransactions), transactionIds);
                            });
                        }
                    });
                }
            } else {
                NRS.getUnconfirmedTransactions(function (unconfirmedTransactions) {
                    NRS.handleIncomingTransactions(unconfirmedTransactions);
                });
            }
        });
    }

    NRS.getUnconfirmedTransactions = function (callback) {
        NRS.sendRequest("getUnconfirmedTransactions", {
            "account": NRS.account
        }, function (response) {
            if (response.unconfirmedTransactions && response.unconfirmedTransactions.length) {
                var unconfirmedTransactions = [];
                var unconfirmedTransactionIds = [];

                response.unconfirmedTransactions.sort(function (x, y) {
                    if (x.timestamp < y.timestamp) {
                        return 1;
                    } else if (x.timestamp > y.timestamp) {
                        return -1;
                    } else {
                        return 0;
                    }
                });

                if (confirmedTransactionIds.length) {
                    NRS.lastTransactions = confirmedTransactionIds.toString();
                }

                NRS.unconfirmedTransactions = unconfirmedTransactions;

                var unconfirmedTransactionIdString = unconfirmedTransactionIds.toString();

                if (unconfirmedTransactionIdString != NRS.unconfirmedTransactionIds) {
                    NRS.unconfirmedTransactionsChange = true;
                    NRS.unconfirmedTransactionIds = unconfirmedTransactionIdString;
                } else {
                    NRS.unconfirmedTransactionsChange = false;
                }

                if (callback) {
                    callback(unconfirmedTransactions);
                } else if (NRS.unconfirmedTransactionsChange) {
                    NRS.incoming.updateDashboardTransactions(unconfirmedTransactions, true);
                }
            } else {
                NRS.unconfirmedTransactions = [];

                if (NRS.unconfirmedTransactionIds) {
                    NRS.unconfirmedTransactionsChange = true;
                } else {
                    NRS.unconfirmedTransactionsChange = false;
                }

                NRS.unconfirmedTransactionIds = "";

                if (callback) {
                    callback([]);
                } else if (NRS.unconfirmedTransactionsChange) {
                    NRS.incoming.updateDashboardTransactions([], true);
                }
            }
        });
    }

    NRS.handleIncomingTransactions = function (transactions, confirmedTransactionIds) {
        var oldBlock = (confirmedTransactionIds === false); //we pass false instead of an [] in case there is no new block..

        if (typeof confirmedTransactionIds != "object") {
            confirmedTransactionIds = [];
        }

        if (confirmedTransactionIds.length) {
            NRS.lastTransactions = confirmedTransactionIds.toString();

            for (var i = transactions.length - 1; i >= 0; i--) {
                if (transactions[i].confirmed) {
                    NRS.lastTransactionsTimestamp = transactions[i].timestamp;
                    break;
                }
            }
        }

        if (confirmedTransactionIds.length || NRS.unconfirmedTransactionsChange) {
            transactions.sort(NRS.sortArray);

            NRS.incoming.updateDashboardTransactions(transactions, confirmedTransactionIds.length == 0);
        }

        //always refresh peers and unconfirmed transactions..
        if (NRS.currentPage == "peers") {
            NRS.incoming.peers();
        } else if (NRS.currentPage == "transactions" && NRS.transactionsPageType == "unconfirmed") {
            NRS.incoming.transactions();
        } else {
            if (!oldBlock || NRS.unconfirmedTransactionsChange) {
                if (NRS.incoming[NRS.currentPage]) {
                    NRS.incoming[NRS.currentPage](transactions);
                }
            }
        }
    }

    NRS.sortArray = function (a, b) {
        return b.timestamp - a.timestamp;
    }

    NRS.incoming.updateDashboardTransactions = function (newTransactions, unconfirmed) {
        var newTransactionCount = newTransactions.length;

        if (newTransactionCount) {
            var rows = "";

            var onlyUnconfirmed = true;

            for (var i = 0; i < newTransactionCount; i++) {
                var transaction = newTransactions[i];

                var receiving = transaction.recipient == NRS.account;
                var account = (receiving ? "sender" : "recipient");

                if (transaction.confirmed) {
                    onlyUnconfirmed = false;
                }

                if (transaction.amountNQT) {
                    transaction.amount = new BigInteger(transaction.amountNQT);
                    transaction.fee = new BigInteger(transaction.feeNQT);
                }

                rows = "<tr class='" + (!transaction.confirmed ? "tentative" : "confirmed") + "'><td><a href='#' data-transaction='" + String(transaction.transaction).escapeHTML() + "' data-timestamp='" + String(transaction.timestamp).escapeHTML() + "'>" + NRS.formatTimestamp(transaction.timestamp) + "</a></td><td style='width:5px;padding-right:0;'>" + (transaction.type == 0 ? (receiving ? "<i class='fa fa-plus-circle' style='color:#65C62E'></i>" : "<i class='fa fa-minus-circle' style='color:#E04434'></i>") : "") + "</td><td><span" + (transaction.type == 0 && receiving ? " style='color:#006400'" : (!receiving && transaction.amount > 0 ? " style='color:red'" : "")) + ">" + NRS.formatAmount(transaction.amount) + "</span> <span" + ((!receiving && transaction.type == 0) ? " style='color:red'" : "") + ">+</span> <span" + (!receiving ? " style='color:red'" : "") + ">" + NRS.formatAmount(transaction.fee) + "</span></td><td>" + NRS.getAccountLink(transaction, account) + "</td><td class='confirmations' data-confirmations='" + String(transaction.confirmations).escapeHTML() + "' data-content='" + (transaction.confirmed ? NRS.formatAmount(transaction.confirmations) + " " + $.t("confirmations") : $.t("unconfirmed_transaction")) + "' data-container='body' data-initial='true'>" + (transaction.confirmations > 10 ? "10+" : String(transaction.confirmations).escapeHTML()) + "</td></tr>" + rows;
            }

            if (onlyUnconfirmed) {
                $("#dashboard_transactions_table tbody tr.tentative").remove();
                $("#dashboard_transactions_table tbody").prepend(rows);
            } else {
                $("#dashboard_transactions_table tbody").empty().append(rows);
            }

            var $parent = $("#dashboard_transactions_table").parent();

            if ($parent.hasClass("data-empty")) {
                $parent.removeClass("data-empty");
                if ($parent.data("no-padding")) {
                    $parent.parent().addClass("no-padding");
                }
            }
        } else if (unconfirmed) {
            $("#dashboard_transactions_table tbody tr.tentative").remove();
        }
    }

    //todo: add to dashboard? 
    NRS.addUnconfirmedTransaction = function (transactionId, callback) {
        NRS.sendRequest("getTransaction", {
            "transaction": transactionId
        }, function (response) {
            if (!response.errorCode) {
                response.transaction = transactionId;
                response.confirmations = "/";
                response.confirmed = false;
                response.unconfirmed = true;

                if (response.attachment) {
                    for (var key in response.attachment) {
                        if (!response.hasOwnProperty(key)) {
                            response[key] = response.attachment[key];
                        }
                    }
                }

                var alreadyProcessed = false;

                try {
                    var regex = new RegExp("(^|,)" + transactionId + "(,|$)");

                    if (regex.exec(NRS.lastTransactions)) {
                        alreadyProcessed = true;
                    } else {
                        $.each(NRS.unconfirmedTransactions, function (key, unconfirmedTransaction) {
                            if (unconfirmedTransaction.transaction == transactionId) {
                                alreadyProcessed = true;
                                return false;
                            }
                        });
                    }
                } catch (e) {
                }

                if (!alreadyProcessed) {
                    NRS.unconfirmedTransactions.unshift(response);
                }

                if (callback) {
                    callback(alreadyProcessed);
                }

                NRS.incoming.updateDashboardTransactions(NRS.unconfirmedTransactions, true);

                NRS.getAccountInfo();
            } else if (callback) {
                callback(false);
            }
        });
    }

    NRS.dateToTimestamp = function (date) {
        var epoch, d;
        epoch = new Date(Date.UTC(2014, 5, 5, 6, 21, 19));
        if (date) {
            d = date;
        } else {
            d = Date.now();
        }
        var t = d - epoch;
        if (t < 0) {
            t = 0;
        } else {
            t = Math.floor(t / 1000);
        }
        return t;
    }

    NRS.pages.transactions = function () {
        if (NRS.transactionsPageType == "unconfirmed") {
            NRS.displayUnconfirmedTransactions();
            return;
        }

        var date1, date2, d;
        if ($("#transactions-period1").is(':checked')) {
            date1 = $("#transactions-datepicker1").datepicker("getDate");
            date2 = new Date(Date.parse($("#transactions-datepicker2").datepicker("getDate")) + 1000 * 60 * 60 * 24 - 1);
        } else if ($("#transactions-period2").is(':checked')) {
            d = new Date();
            d.setHours(23);
            d.setMinutes(59);
            d.setSeconds(59);
            d.setMilliseconds(999);
            date1 = new Date(d - $("#transactions-period2-input").val() * 24 * 60 * 60 * 1000 + 1);
            date2 = d;
        } else
            return;
        $("#transactions-date_before").html(NRS.formatTimestamp(NRS.dateToTimestamp(date1), true));
        $("#transactions-date_after").html(NRS.formatTimestamp(NRS.dateToTimestamp(date2), true));

        var rows = "";

        var params = {
            "account": NRS.account,
            "firstIndex": 0,
            "lastIndex": 9999,
            "timestamp": NRS.dateToTimestamp(date1)
        };

        if (NRS.transactionsPageType) {
            params.type = NRS.transactionsPageType.type;
            params.subtype = NRS.transactionsPageType.subtype;
            var unconfirmedTransactions = NRS.getUnconfirmedTransactionsFromCache(params.type, params.subtype);
        } else {
            var unconfirmedTransactions = NRS.unconfirmedTransactions;
        }

        if (unconfirmedTransactions) {
            for (var i = 0; i < unconfirmedTransactions.length; i++) {
                rows = NRS.getTransactionRowHTML(unconfirmedTransactions[i]) + rows;
            }
        }

        var balance1, balance2;
        NRS.sendRequest("getAccountTransactions+", params, function (response) {
            if (response.transactions) {
                NRS.sendRequest("getBalance", {"account": NRS.account}, function (re) {
                    if (!re.errorCode) {
                        balance2 = new BigInteger(re.balanceNQT);
                        balance1 = new BigInteger(re.balanceNQT);
                        if (response.transactions.length) {
                            for (var i = 0; i < response.transactions.length; i++) {
                                var transaction = response.transactions[i];

                                transaction.confirmed = true;
                                var tcost = NRS.getTransactionCost(transaction);

                                if (transaction.timestamp > NRS.dateToTimestamp(date2)) {
                                    balance1 = balance1.subtract(tcost);
                                    balance2 = balance2.subtract(tcost);
                                } else if (NRS.dateToTimestamp(date1) <= transaction.timestamp && transaction.timestamp <= NRS.dateToTimestamp(date2)) {
                                    rows = NRS.getTransactionRowHTML(transaction) + rows;
                                    balance1 = balance1.subtract(tcost);
                                }
                            }
                        }
                        $("#transactions-balance1").html(NRS.formatStyledAmount(balance1, false, 10, true));
                        $("#transactions-balance2").html(NRS.formatStyledAmount(balance2, false, 10, true));
                    } else {
                        //alert("error getting balance") //TODO
                    }
                    NRS.dataLoaded(rows);
                });

            } else {
                //alert("error loading transactions") // TODO
                NRS.dataLoaded(rows);
            }

        });
    }

    NRS.incoming.transactions = function (transactions) {
        NRS.loadPage("transactions");
    }

    NRS.displayUnconfirmedTransactions = function () {
        NRS.sendRequest("getUnconfirmedTransactions", function (response) {
            var rows = "";

            if (response.unconfirmedTransactions && response.unconfirmedTransactions.length) {
                for (var i = 0; i < response.unconfirmedTransactions.length; i++) {
                    rows = NRS.getTransactionRowHTML(response.unconfirmedTransactions[i]) + rows;
                }
            }

            NRS.dataLoaded(rows);
        });
    }

    NRS.getTransactionRowHTML = function (transaction) {
        var transactionType = $.t("unknown");

        var proceed = false;

        if (transaction.type == 0) {
            transactionType = $.t("ordinary_payment");
            proceed = true;
        } else if (transaction.type == 1) {
            switch (transaction.subtype) {
                case 0:
                    transactionType = $.t("arbitrary_message");
                    proceed = true;
                    break;
                case 1:
                    transactionType = $.t("alias_assignment");
                    break;
                case 2:
                    transactionType = $.t("poll_creation");
                    break;
                case 3:
                    transactionType = $.t("vote_casting");
                    break;
                case 4:
                    transactionType = $.t("hub_announcements");
                    break;
                case 5:
                    transactionType = $.t("account_info");
                    proceed = true;
                    break;
                case 6:
                    if (transaction.attachment.priceNQT == "0") {
                        if (transaction.sender == NRS.account && transaction.recipient == NRS.account) {
                            transactionType = $.t("alias_sale_cancellation");
                        } else {
                            transactionType = $.t("alias_transfer");
                        }
                    } else {
                        transactionType = $.t("alias_sale");
                    }
                    break;
                case 7:
                    transactionType = $.t("alias_buy");
                    break;
            }
        } else if (transaction.type == 2) {
            switch (transaction.subtype) {
                case 0:
                    transactionType = $.t("asset_issuance");
                    break;
                case 1:
                    transactionType = $.t("asset_transfer");
                    break;
                case 2:
                    transactionType = $.t("ask_order_placement");
                    break;
                case 3:
                    transactionType = $.t("bid_order_placement");
                    break;
                case 4:
                    transactionType = $.t("ask_order_cancellation");
                    break;
                case 5:
                    transactionType = $.t("bid_order_cancellation");
                    break;
            }
        } else if (transaction.type == 3) {
            switch (transaction.subtype) {
                case 0:
                    transactionType = $.t("marketplace_listing");
                    break;
                case 1:
                    transactionType = $.t("marketplace_removal");
                    break;
                case 2:
                    transactionType = $.t("marketplace_price_change");
                    break;
                case 3:
                    transactionType = $.t("marketplace_quantity_change");
                    break;
                case 4:
                    transactionType = $.t("marketplace_purchase");
                    break;
                case 5:
                    transactionType = $.t("marketplace_delivery");
                    break;
                case 6:
                    transactionType = $.t("marketplace_feedback");
                    break;
                case 7:
                    transactionType = $.t("marketplace_refund");
                    break;
            }
        } else if (transaction.type == 4) {
            switch (transaction.subtype) {
                case 0:
                    transactionType = $.t("balance_leasing");
                    break;
            }
        }

        if (!proceed)
            return "";

        var receiving = transaction.recipient == NRS.account;
        var account = (receiving ? "sender" : "recipient");

        if (transaction.amountNQT) {
            transaction.amount = new BigInteger(transaction.amountNQT);
            transaction.fee = new BigInteger(transaction.feeNQT);
        }

        var transactionMessage = "";
        var pwneeded = false;
        if (transaction.attachment) {
            var decoded = "";
            if (transaction.attachment.message) {
                transactionMessage += "<i class='fa fa-unlock-alt symbol-gray'></i> " + transaction.attachment.message;
            } else if (transaction.attachment.encryptedMessage) {

                try {
                    decoded = NRS.tryToDecryptMessage(transaction);
                    transactionMessage += "<i class='fa fa-lock symbol-gray'></i> "
                } catch (err) {
                    if (err.errorCode && err.errorCode == 1) {
                        //decoded = $.t("error_decryption_passphrase_required");
                        decoded = " ";
                        pwneeded = true;
                    } else {
                        decoded = $.t("error_decryption_unknown");
                    }
                }

                transactionMessage += decoded;
            }
            if (transaction.sender == NRS.account && transaction.attachment.encryptToSelfMessage) {
                if (decoded != "") {
                    transactionMessage += "<br>";
                }
                decoded = "";

                try {
                    decoded = NRS.tryToDecryptNote(transaction);
                } catch (err) {
                    if (err.errorCode && err.errorCode == 1) {
                        //decoded = $.t("error_decryption_passphrase_required");
                        decoded = " ";
                        pwneeded = true;
                    } else {
                        decoded = $.t("error_decryption_unknown");
                    }
                }

                transactionMessage += "<i>" + decoded + "</i>";

            }
            if (pwneeded) {
                transactionMessage = "<a href='#' data-transaction='" + String(transaction.transaction).escapeHTML() + "'><i class='fa fa-lock symbol-gray'></i>" + transactionMessage + "</a>";
            }
        }

        var amountFeeColumn = "<td class='transactions-lastcol' ";
        if (transaction.type == 0) {
            if (receiving) {
                amountFeeColumn += "style='color:#006400;'>" + NRS.formatAmount(transaction.amount);
            } else {
                amountFeeColumn += "style='color:red'>" + NRS.formatAmount(transaction.amount) + " + " + NRS.formatAmount(transaction.fee);
            }
        } else if (!receiving) {
            amountFeeColumn += "style='color:red'>" + NRS.formatAmount(transaction.fee);
        } else {
            amountFeeColumn += ">0";
        }
        amountFeeColumn += "</td>";

        return "<tr " + (!transaction.confirmed && (transaction.recipient == NRS.account || transaction.sender == NRS.account) ? " class='tentative'" : "") + "><td>" + NRS.formatTimestamp(transaction.timestamp) + "</td><td><a href='#' data-transaction='" + String(transaction.transaction).escapeHTML() + "'>" + transactionType + "</a></td><td class='transactions-info_note'>" + transactionMessage + "</td><td>" + NRS.getAccountLink(transaction, account) + "</td><td style='width:5px;padding-right:0;'>" + (transaction.type == 0 ? (receiving ? "<i class='fa fa-plus-circle' style='color:#65C62E'></i>" : "<i class='fa fa-minus-circle' style='color:#E04434'></i>") : "") + "</td>" + amountFeeColumn + "</tr>";
    }

    NRS.getTransactionCost = function (transaction) {
        var r;
        if (transaction.recipient == NRS.account) {
            r = new BigInteger(transaction.amountNQT);
        } else {
            r = new BigInteger(transaction.amountNQT);
            var r2 = new BigInteger(transaction.feeNQT);
            r = r.add(r2).multiply(new BigInteger("-1"));
        }

        return r;
    }


    $("#transactions_page_type li a").click(function (e) {
        e.preventDefault();

        var type = $(this).data("type");

        if (!type) {
            NRS.transactionsPageType = null;
        } else if (type == "unconfirmed") {
            NRS.transactionsPageType = "unconfirmed";
        } else {
            type = type.split(":");
            NRS.transactionsPageType = {
                "type": type[0],
                "subtype": type[1]
            };
        }

        $(this).parents(".btn-group").find(".text").text($(this).text());

        $(".popover").remove();

        NRS.loadPage("transactions");
    });

    return NRS;
}(NRS || {}, jQuery));
