/**
 * @depends {nrs.js}
 */
var NRS = (function(NRS, $, undefined) {
	NRS.defaultSettings = {
		"submit_on_enter": 0,
		"animate_forging": 1,
		"news": -1,
		"console_log": 0,
		"fee_warning": "1000000000000",
		"amount_warning": "10000000000000",
		"asset_transfer_warning": "10000",
		"24_hour_format": 1,
		"remember_passphrase": 0,
		"language": "en"
	};

	NRS.defaultColors = {
		"header": "#408EBA",
		"sidebar": "#F4F4F4",
		"boxes": "#fff"
	};

	NRS.pages.settings = function() {
		for (var key in NRS.settings) {
			if (/_warning/i.test(key) && key != "asset_transfer_warning") {
				if ($("#settings_" + key).length) {
					$("#settings_" + key).val(NRS.convertToNXT(NRS.settings[key]));
				}
			} else if (!/_color/i.test(key)) {
				if ($("#settings_" + key).length) {
					$("#settings_" + key).val(NRS.settings[key]);
				}
			}
		}

		if (NRS.settings["news"] != -1) {
			$("#settings_news_initial").remove();
		}

		if (NRS.inApp) {
			$("#settings_console_log_div").hide();
		}

		NRS.pageLoaded();
	}

	function getCssGradientStyle(start, stop, vertical) {
		var output = "";

		var startPosition = (vertical ? "left" : "top");

		output += "background-image: -moz-linear-gradient(" + startPosition + ", " + start + ", " + stop + ");";
		output += "background-image: -ms-linear-gradient(" + startPosition + ", " + start + ", " + stop + ");";
		output += "background-image: -webkit-gradient(linear, " + (vertical ? "left top, right top" : "0 0, 0 100%") + ", from(" + start + "), to(" + stop + "));";
		output += "background-image: -webkit-linear-gradient(" + startPosition + ", " + start + ", " + stop + ");";
		output += "background-image: -o-linear-gradient(" + startPosition + ", " + start + ", " + stop + ");";
		output += "background-image: linear-gradient(" + startPosition + ", " + start + ", " + stop + ");";
		output += "filter: progid:dximagetransform.microsoft.gradient(startColorstr='" + start + "', endColorstr='" + stop + "', GradientType=" + (vertical ? "1" : "0") + ");";
		return output;
	}

	NRS.getSettings = function() {
		if (NRS.databaseSupport) {
			NRS.database.select("data", [{
				"id": "settings"
			}], function(error, result) {
				if (result && result.length) {
					NRS.settings = $.extend({}, NRS.defaultSettings, JSON.parse(result[0].contents));
				} else {
					NRS.database.insert("data", {
						id: "settings",
						contents: "{}"
					});
					NRS.settings = NRS.defaultSettings;
				}
				NRS.applySettings();
			});
		} else {
			if (NRS.hasLocalStorage) {
				NRS.settings = $.extend({}, NRS.defaultSettings, JSON.parse(localStorage.getItem("settings")));
			} else {
				NRS.settings = NRS.defaultSettings;
			}
			NRS.applySettings();
		}
	}

	NRS.applySettings = function(key) {
		if (!key || key == "language") {
			if ($.i18n.lng() != NRS.settings["language"]) {
				$.i18n.setLng(NRS.settings["language"], null, function() {
					$("[data-i18n]").i18n();
				});
				if (key && window.localstorage) {
					window.localStorage.setItem('i18next_lng', NRS.settings["language"]);
				}
				if (NRS.inApp) {
					parent.postMessage({
						"type": "language",
						"version": NRS.settings["language"]
					}, "*");
				}
				
			}
			$( "#transactions-datepicker1, #transactions-datepicker2" ).datepicker( "option", $.datepicker.regional[ NRS.settings["language"] ] );
		}

		if (!key || key == "submit_on_enter") {
			if (NRS.settings["submit_on_enter"]) {
				$(".modal form:not('#decrypt_note_form_container')").on("submit.onEnter", function(e) {
					e.preventDefault();
					NRS.submitForm($(this).closest(".modal"));
				});
			} else {
				$(".modal form").off("submit.onEnter");
			}
		}

		if (!key || key == "animate_forging") {
			if (NRS.settings["animate_forging"]) {
				$("#forging_indicator").addClass("animated");
			} else {
				$("#forging_indicator").removeClass("animated");
			}
		}

		if (!key || key == "news") {
			if (NRS.settings["news"] == 0) {
				$("#news_link").hide();
			} else if (NRS.settings["news"] == 1) {
				$("#news_link").show();
			}
		}

		if (!NRS.inApp && !NRS.downloadingBlockchain) {
			if (!key || key == "console_log") {
				if (NRS.settings["console_log"] == 0) {
					$("#show_console").hide();
				} else {
					$("#show_console").show();
				}
			}
		} else if (NRS.inApp) {
			$("#show_console").hide();
		}

		if (key == "24_hour_format") {
			var $dashboard_dates = $("#dashboard_transactions_table a[data-timestamp], #dashboard_blocks_table td[data-timestamp]");

			$.each($dashboard_dates, function(key, value) {
				$(this).html(NRS.formatTimestamp($(this).data("timestamp")));
			});
		}

		if (!key || key == "remember_passphrase") {
			if (NRS.settings["remember_passphrase"] == 1) {
				NRS.setCookie("remember_passphrase", 1, 1000);
			} else {
				NRS.deleteCookie("remember_passphrase");
			}
		}
	}

	NRS.updateSettings = function(key, value) {
		if (key) {
			NRS.settings[key] = value;
		}

		if (NRS.databaseSupport) {
			NRS.database.update("data", {
				contents: JSON.stringify(NRS.settings)
			}, [{
				id: "settings"
			}]);
		} else if (NRS.hasLocalStorage) {
			localStorage.setItem("settings", JSON.stringify(NRS.settings));
		}

		NRS.applySettings(key);
	}

	$("#settings_box select").on("change", function(e) {
		e.preventDefault();

		var key = $(this).attr("name");
		var value = $(this).val();

		NRS.updateSettings(key, value);
	});

	$("#settings_box input[type=text]").on("input", function(e) {
		var key = $(this).attr("name");
		var value = $(this).val();

		if (/_warning/i.test(key) && key != "asset_transfer_warning") {
			value = NRS.convertToNQT(value);
		}
		NRS.updateSettings(key, value);
	});

	return NRS;
}(NRS || {}, jQuery));
