This release includes a critical bugfix release. Everyone using NFD 1.2.6 or earlier should upgrade!

NFD 1.2.9 changelog:
 * New API request getRichList. Example: http://localhost:8876/nxt?requestType=getRichList
 * New API request getAllAccountIds. Example:  http://localhost:8876/nxt?requestType=getAllAccountIds
 * Fixed: a wrong jquery reference which prevented the block explorer from working
 * NFD NSC asset transfer tool as code example
 * Enabling re-validation of blocks and transactions at start (nxt.forceValidate=true). Validation will be disabled again with 1.3.x release. 
 * Critical bugfix release (please see nxt-client-1.2.9.changelog.txt) Everyone using NFD 1.2.6 or earlier should upgrade!
 * Version scheme changed to NXT like version scheme

Merged all features and bugfixes from NXT. Please look at the NXT client changelog files for details:

 * changelogs/nxt-client-1.2.7.changelog.txt 	
 * changelogs/nxt-client-1.2.8.changelog.txt
 * changelogs/nxt-client-1.2.9.changelog.txt