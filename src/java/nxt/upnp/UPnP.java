package nxt.upnp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import org.xml.sax.SAXException;

import nxt.Constants;
import nxt.Nxt;
import nxt.util.Logger;

public class UPnP {

	private static GatewayDiscover gatewayDiscover = new GatewayDiscover();
	private static GatewayDevice activeGW;
	private static String myAddress = null;
	private static final boolean upnpEnabled = Nxt.getBooleanProperty("nxt.enableUPnP");
	static final int port = Constants.isTestnet ? 9874 : Nxt.getIntProperty("nxt.peerServerPort");
	
	public static void shutdown() {
		if (!upnpEnabled)
			return;
		try {
			if (activeGW != null) {
				activeGW.deletePortMapping(port, "TCP");
				Logger.logDebugMessage("UPnP mapping removed.");
			}
		} catch (IOException | SAXException e) {
			Logger.logDebugMessage("Could not remove UPnP mapping.");
		}

	}

	public static String getMyAddress(){
		return myAddress;
	}
	
	public static void init() {
		if (!upnpEnabled)
			return;

		Logger.logMessage("UPnP is enabled, looking for gateway devices...");

		try {
			// get a gateway with an external address
			activeGW = getGateway();
			if (activeGW == null) {
				Logger.logMessage("UPnP: No usable gateway device found.");
				return;
			}

			InetAddress localAddress = activeGW.getLocalAddress();
			Logger.logDebugMessage("Using local address: " + localAddress.getHostAddress());

			PortMappingEntry portMapping = new PortMappingEntry();
			portMapping.setRemoteHost(activeGW.getExternalIPAddress());
			portMapping.setExternalPort(port);
			portMapping.setProtocol("TCP");

			if (activeGW.getGenericPortMappingEntry(0, portMapping)) {
				if (portMapping.getInternalClient().equals(activeGW.getLocalAddress().getHostAddress())
						&& portMapping.getInternalPort() == port) {
					myAddress=new String(activeGW.getExternalIPAddress()+":"+port);
					Logger.logMessage("Port already mapped, nothing to do.");
					return;
				} else {
					activeGW.deletePortMapping(port, "TCP");
				}
			}

			if (activeGW.addPortMapping(port, port, localAddress.getHostAddress(), "TCP", "NFD")) {
				Logger.logMessage("External address " + activeGW.getExternalIPAddress() + ":" + port + " mapped to "
						+ localAddress.getHostAddress() + ":" + port);
				myAddress=new String(activeGW.getExternalIPAddress()+":"+port);
			}
		} catch (Exception e) {
			Logger.logErrorMessage("UPnP detection failed!");
		}
	}

	private static GatewayDevice getGateway() throws Exception {
		Map<InetAddress, GatewayDevice> gateways = gatewayDiscover.discover();

		for (GatewayDevice gatewayDevice : gateways.values()) {
			if (isExternalAddress(gatewayDevice.getExternalIPAddress()))
				return gatewayDevice;
		}
		return null;
	}

	private static boolean isExternalAddress(String ip) {
		InetAddress address = null;
		try {
			address = java.net.InetAddress.getByName(ip);
		} catch (UnknownHostException e1) {
			return false;
		}
		if (address != null
				&& (address.isAnyLocalAddress() || address.isLoopbackAddress() || address.isMulticastAddress())) {
			return false;
		}
		return true;
	}

}
